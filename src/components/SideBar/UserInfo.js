import React, { Component } from 'react';
import './../../assets/styles/base.scss';

class UserInfo extends Component {
  render() {
    return (
      <div className="user-wrapper">
        <div className="user">
          <div className="userinfo">
            <div className="username">
              {"Akzharkyn Sagidullina"}
            </div>
            <div className="title">Admin</div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserInfo;