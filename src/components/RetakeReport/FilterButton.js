import React, { Component } from 'react';
import {Button} from 'react-bootstrap';

class FilterButton extends Component {

  handleClick() {
      alert('А поиск не работает)');
  }
    render() {
        return (
          <div>
            <Button variant="primary" type="submit" className = "btn-submit filter-button"
              onClick={(event) => this.handleClick(event)}>Поиск</Button>
          </div>
        );
    }
}
export default FilterButton;
