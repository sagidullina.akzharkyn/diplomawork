import React, { Component } from 'react';
import {Button} from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

class RegisteredStudentsReportTable extends Component {

  render() {
    const options = {
      sizePerPage: 20,
      //prePage: 'Previous',
      //nextPage: 'Next',
      hideSizePerPage: true
    };

    let registeredStudents = this.props.registeredStudents;

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">

              <div className="content">
              <BootstrapTable
                data={registeredStudents}
                bordered={false}
                striped
                options={options}>

                <TableHeaderColumn
                  dataField='studentId'
                  isKey
                  width="10%">
                  ID
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='lastName'
                  width="15%">
                  Фaмилия
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='firstName'
                  width="10%">
                  Имя
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='specialty'
                  width="20%">
                  Специальность
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='year'
                  width="10%">
                  Учебный год
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='semester'
                  width="10%">
                  Семестр
                </TableHeaderColumn>
                <TableHeaderColumn
                  dataField='subject'
                  width="25%">
                  Предмет
                </TableHeaderColumn>

              </BootstrapTable>
              </div>
              <div className="pagination">
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickPrevious(event)}>Previous</Button>
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickNext(event)}>Next</Button>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default RegisteredStudentsReportTable;
