import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';

class BarGraph extends Component {

  render() {

    let retakeGraphInfoLabel = this.props.retakeGraphInfo.map((infoByFaculty)=>
      infoByFaculty.label
    );

    let retakeGraphInfoCount = this.props.retakeGraphInfo.map((infoByFaculty)=>
      infoByFaculty.count
    );

    console.log(retakeGraphInfoLabel);
    console.log(retakeGraphInfoCount);

    const data = {
        labels: retakeGraphInfoLabel,
        datasets: [
            {
                label: 'Количество ретейков за семестр',
                fill: false,
                lineTension: 0.1,
                backgroundColor: '#FFBB00',
                borderColor: '#FFBB00',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: retakeGraphInfoCount
            }
        ]
    };

  return (
    <div className="filters">
      <Bar data={data}/>
    </div>
  )
}
}

export default BarGraph;
