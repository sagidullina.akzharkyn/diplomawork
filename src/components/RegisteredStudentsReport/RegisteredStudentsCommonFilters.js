import React, { Component } from 'react';
import {Input, Label, FormGroup} from 'reactstrap';
import {Button} from 'react-bootstrap';
import './registeredStudentsCommonFilters.css';

const DOMAIN = 'http://18.222.209.77'

class CommonFilters extends Component {

    constructor(props) {
      super(props)
      this.state = {
        semesters: [],
        faculties: [],
        years: [],
        specialties: [],
        courses: [1,2,3,4],
        year_id: 1,
        semester_id: 1,
        faculty_id: 1,
        specialty_id: 1,
        course_id: 1,
        year_name: '',
        semester_name: '',
        faculty_name: '',
        //validated: false,
        error: null
      }
      this.handleYearSelect = this.handleYearSelect.bind(this)
      this.handleSemesterSelect = this.handleSemesterSelect.bind(this)
      this.handleFacultySelect = this.handleFacultySelect.bind(this)
      this.handleSpecialtySelect = this.handleSpecialtySelect.bind(this)
      this.handleCourseSelect = this.handleCourseSelect.bind(this)

    }



    handleClick() {
      //fetch by sending state vars + value of pagination
        var semester_id = this.state.semester_id
        var year_id = this.state.year_id
        var faculty_id = this.state.faculty_id
        var specialty_id = this.state.specialty_id
        var course_id = this.state.course_id


        console.log("year_id: " + this.state.year_id +
          " sem_id: " + this.state.semester_id + " fac_id: " + this.state.faculty_id +
            " spec_id: " + this.state.specialty_id + " course_id: " + this.state.course_id)
        this.props.fetchResultFromFilters(year_id,faculty_id,semester_id, specialty_id, course_id)
    }

    handleUpload() {
      //fetch by sending state vars + value of pagination
      var semester_id = this.state.semester_id
      var year_id = this.state.year_id
      var faculty_id = this.state.faculty_id
      var specialty_id = this.state.specialty_id
      var course_id = this.state.course_id
      var year_name = this.state.year_name
      var semester_name = this.state.semester_name
      var faculty_name = this.state.faculty_name

      console.log("year_id: " + this.state.year_id +
        " sem_id: " + this.state.semester_id + " fac_id: " + this.state.faculty_id +
          " spec_id: " + this.state.specialty_id + " course_id: " + this.state.course_id)
      this.props.getUploadedFile(year_id,faculty_id,semester_id, specialty_id, course_id, year_name, semester_name, faculty_name)
    }

    handleYearSelect(event) {
      //console.log(event.target.selectedIndex);
      var yearName = event.target[event.target.selectedIndex].value
      this.setState({
        year_id: event.target[event.target.selectedIndex].id,
        year_name: yearName,
      },()=> {this.props.refreshPageCount()});


    }

    handleSemesterSelect(event) {
      //console.log(event.target.selectedIndex);
      var semesterName = event.target[event.target.selectedIndex].value
      this.setState({
        semester_id: event.target[event.target.selectedIndex].id,
        semester_name: semesterName
      },()=> {this.props.refreshPageCount()});
    }

    handleFacultySelect(event) {
      //console.log(event.target.selectedIndex);
      var facultyName = event.target[event.target.selectedIndex].value
      this.setState({
        faculty_id: event.target[event.target.selectedIndex].id,
        faculty_name: facultyName,
      },()=> {this.props.refreshPageCount()});

      const fac_id = event.target[event.target.selectedIndex].id
      console.log(fac_id );

      fetch(DOMAIN + '/api/filter/specialties?facultyId=' + fac_id ,
      )
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({specialties:result.data})
          console.log(result.data)
        },
        (error) => {
            this.setState({error})
        }
      )

    }
    handleSpecialtySelect(event) {
      //console.log(event.target.selectedIndex);
      this.setState({
        specialty_id: event.target[event.target.selectedIndex].id
      },()=> {this.props.refreshPageCount()});
    }

    handleCourseSelect(event) {
      //console.log(event.target.selectedIndex);
      this.setState({
        course_id: event.target[event.target.selectedIndex].id
      },()=> {this.props.refreshPageCount()});
    }

    componentDidMount() {
      fetch(DOMAIN + '/api/filter/semesters',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({semesters:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )
        fetch(DOMAIN + '/api/filter/faculties',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({faculties:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )
        fetch(DOMAIN + '/api/filter/years',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({years:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )

    }

    render() {
      let semesters = this.state.semesters.map((semester)=> {
            return (
            <option key = {semester.id} id = {semester.id}>
            {semester.name}
            </option>)
        });
        let years = this.state.years.map((year)=> {
          return (
          <option key = {year.id} id = {year.id} >
          {year.name}
          </option>)
      });
      let faculties = this.state.faculties.map((faculty)=> {
        return (
        <option key = {faculty.id} id = {faculty.id}>
        {faculty.name}
        </option>)
      });
      let specialties = this.state.specialties.map((specialty)=> {
        return (
        <option key = {specialty.id} id = {specialty.id}>
        {specialty.name}
        </option>)
      });
      let courses = this.state.courses.map((course)=> {
        return (
        <option key = {course} id = {course}>
        {course}
        </option>)
      });

        return (
          <div className="all-filters">
            <div className="common-filter">
              <div className="common-filter-1">
                <FormGroup className="academic-year-input" >
                  <Label for="exampleSelect">Учебный год</Label>
                  <Input type="select" name="yearInput" id="exampleSelect" required
                  onChange={this.handleYearSelect}>
                  <option key='0' id = '0' disabled >Выберите учебный год</option>
                    {years}
                  </Input>
                </FormGroup>
                <FormGroup className="semester-inpu">
                  <Label for="exampleSelect">Семестр</Label>
                  <Input type="select" name="semesterInput" id="exampleSelect"
                  onChange={this.handleSemesterSelect}>
                  <option key='0' disabled >Выберите семестр</option>
                    {semesters}
                  </Input>
                </FormGroup>
                <FormGroup className="course-input">
                  <Label for="exampleSelect">Курсы</Label>
                  <Input type="select" name="courseInput" id="exampleSelect"
                  onChange={this.handleCourseSelect}>
                    <option key='0' disabled >Выберите специальность</option>
                    {courses}
                  </Input>
                </FormGroup>
              </div>

              <div className="common-filter-2">
                <FormGroup className="faculty-input">
                  <Label for="exampleSelect">Факультеты</Label>
                  <Input type="select" name="facultyInput" id="exampleSelect"
                  onChange={this.handleFacultySelect}>
                    <option key='0' disabled >Выберите факультет</option>
                    {faculties}
                  </Input>
                </FormGroup>
                <FormGroup className="specialty-input">
                  <Label for="exampleSelect">Специальности</Label>
                  <Input type="select" name="specialtyInput" id="exampleSelect"
                  onChange={this.handleSpecialtySelect}>
                    <option key='0' disabled >Выберите специальность</option>
                    {specialties}
                  </Input>
                </FormGroup>
              </div>
            </div>

              <div className="filter-download-button">
              <div>
                <Button variant="info" type="submit" className = "btn-submit filter-button"
                onClick={() => this.handleClick()}>Поиск</Button>
              </div>
              <div>
                <Button variant="outline-success" type="submit" className = "btn-submit filter-button"
                onClick={() => this.handleUpload()}>Выгрузить отчет</Button>
              </div>
              </div>
            </div>
        );
    }
}
export default CommonFilters;
