import React, { Component } from 'react';
import {Route} from 'react-router-dom';

import AuthService from './AuthService';
import withAuth from './withAuth';

import Header from './Header';
import SideBar from './SideBar';


import NotAllowedByPointsReport from './NotAllowedByPointsReport/NotAllowedByPointsReport.js';
import SemesterPerformanceReport from './SemesterPerformanceReport/SemesterPerformanceReport.js';
import LostScholarshipsReport from './LostScholarshipsReport/LostScholarshipsReport.js';
import RegisteredStudentsReport from './RegisteredStudentsReport/RegisteredStudentsReport.js';
import RetakeReport from './RetakeReport/RetakeReport.js';
import RetakeGraph from './RetakeGraph/RetakeGraph.js';

const Auth = new AuthService();

class Main extends Component {
  constructor(props) {
    super(props);
    this.loggingOut = this.loggingOut.bind(this)
  }

  loggingOut() {
    Auth.logout()
    this.props.history.replace('/login');
    console.log(localStorage.getItem('id_token'));
  }

  render() {
    return (
        <div className="wrapp">
          <SideBar />
        <div className="main-panel">
            <Header handleLogout = {this.loggingOut} />
           {/* routes to report tables  */}
           <Route path = "/main/retake-report" component = {RetakeReport} />
           <Route path = "/main/notallowed-bypoints-report" component = {NotAllowedByPointsReport} />
           <Route path = "/main/semester-performance-report" component = {SemesterPerformanceReport} />
           <Route path = "/main/lost-scholarships-report" component = {LostScholarshipsReport} />
           <Route path = "/main/registered-students-report" component = {RegisteredStudentsReport} />
           <Route path = "/main/retake-graph" component = {RetakeGraph} />
        </div>
      </div>
    );
  }
}

export default withAuth(Main);


  