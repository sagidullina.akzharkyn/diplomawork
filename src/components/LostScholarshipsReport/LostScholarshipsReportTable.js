import React, { Component } from 'react';
//import generateData from './generateData';
//import retakers from "./Mockdata/retakers.json";
import {Button} from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

class LostScholarshipsReportTable extends Component {

  render() {
    //pagination={true}
    const options = {
      sizePerPage: 20,
      prePage: 'Previous',
      nextPage: 'Next',
      //firstPage: 'First',
      //lastPage: 'Last',
      hideSizePerPage: true,
    };
    let loastScholarships = this.props.loastScholarships
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div >

              <div className="content">
                <BootstrapTable
                  data={loastScholarships}
                  bordered={false}
                  striped
                  options={options}>

                  <TableHeaderColumn
                    dataField='studentId'
                    isKey
                    width="10%">
                    ID
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='lastName'
                    width="15%">
                    Фaмилия
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='firstName'
                    width="10%">
                    Имя
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='subject'
                    width="25%">
                    Предмет
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='grade1'
                    width="10%">
                    1-а
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='grade2'
                    width="10%">
                    2-а
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='finalGrade'
                    width="10%">
                    Экзамен
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='totalGrade'
                    width="10%">
                    Общая
                  </TableHeaderColumn>
                </BootstrapTable>
              </div>
              <div className="pagination page-buttons">
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickPrevious(event)}>Previous</Button>
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickNext(event)}>Next</Button>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default LostScholarshipsReportTable;
