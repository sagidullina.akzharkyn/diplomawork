import React, { Component } from 'react';
import CommonFilters from './CommonFilters';
import BarGraph from './BarGraph';
import Empty from './Empty';

const DOMAIN = 'http://18.222.209.77';

class RetakeGraph extends Component {

  constructor(props) {
    super(props)
    this.state = {
      retakeGraphInfo: [],
      faculty_id: 0,
      barGraph: false
    }
    this.handleFilterResults= this.handleFilterResults.bind(this);
    this.handleFetch= this.handleFetch.bind(this);
    this.showBarGraph = this.showBarGraph.bind(this);
  }

  handleFilterResults(facultyId) {
    this.setState({
      faculty_id: facultyId,
      barGraph: true
    }, () => {
      this.handleFetch()
    })
  }

  handleFetch() {

    var faculty_id = this.state.faculty_id

    fetch(DOMAIN + '/api/graph/retake?&facultyId=' + faculty_id)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({retakeGraphInfo:result.data})
          }
    )
  }

  showBarGraph() {

		this.setState({barGraph: true});

	}

  render() {
    let shownBarGraph = null;
		if(this.state.barGraph===true) {

			shownBarGraph = <BarGraph retakeGraphInfo = {this.state.retakeGraphInfo}/>
			//console.log(this.state.show.foods)
		}
    else{
      shownBarGraph = <Empty/>
    }
    return (
        <div className="container-fluid">
          <div className="report-title">
            <h4>График количества ретейков за семестр</h4>
          </div>
          <div className="filters card">
          <CommonFilters fetchResultFromFilters = {this.handleFilterResults}/>
          </div>
        <div className="row">
          <div className="col-md-12">
            {shownBarGraph}
          </div>
        </div>
      </div>
    )
  }
}

export default RetakeGraph;
