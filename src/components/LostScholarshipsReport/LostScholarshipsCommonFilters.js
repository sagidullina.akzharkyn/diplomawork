import React, { Component } from 'react';
import {Input, Label, FormGroup} from 'reactstrap';
import {Button } from 'react-bootstrap';
import './lostScholarshipsCommonFilters.css';
//import semesters from './Mockdata/semesters.json';
//import years from './Mockdata/years.json';
//import faculties from './Mockdata/faculties.json';

const DOMAIN = 'http://18.222.209.77'

class CommonFilters extends Component {

    constructor(props) {
      super(props)
      this.state = {
        semesters: [],
        faculties: [],
        years: [],
        year_id: 1,
        semester_id: 1,
        faculty_id: 1,
        year_name: '',
        semester_name: '',
        faculty_name: '',
        error: null
      }
      this.handleYearSelect = this.handleYearSelect.bind(this)
      this.handleSemesterSelect = this.handleSemesterSelect.bind(this)
      this.handleFacultySelect = this.handleFacultySelect.bind(this)

    }

    handleClick() {
      //fetch by sending state vars + value of pagination
        var semester_id = this.state.semester_id
        var year_id = this.state.year_id
        var faculty_id = this.state.faculty_id
        console.log("year_id: " + this.state.year_id +
        " sem_id: " + this.state.semester_id + " fac_id: " + this.state.faculty_id)
        this.props.fetchResultFromFilters(year_id,faculty_id,semester_id)
    }

    handleUpload() {
      //fetch by sending state vars + value of pagination
      var semester_id = this.state.semester_id
      var year_id = this.state.year_id
      var faculty_id = this.state.faculty_id
      var year_name = this.state.year_name
      var semester_name = this.state.semester_name
      var faculty_name = this.state.faculty_name

      console.log("year_id: " + this.state.year_id +
        " sem_id: " + this.state.semester_id + " fac_id: " + this.state.faculty_id )
      this.props.getUploadedFile(year_id,faculty_id,semester_id, year_name, semester_name, faculty_name)
    }

    handleYearSelect(event) {
      //console.log(event.target.selectedIndex);
      var yearName = event.target[event.target.selectedIndex].value

      this.setState({
        year_id: event.target[event.target.selectedIndex].id,
        year_name: yearName,
      },()=> {this.props.refreshPageCount()});
    }
    handleSemesterSelect(event) {
      //console.log(event.target.selectedIndex);
      var semesterName = event.target[event.target.selectedIndex].value
      this.setState({
        semester_id: event.target[event.target.selectedIndex].id,
        semester_name: semesterName
      },()=> {this.props.refreshPageCount()});
    }
    handleFacultySelect(event) {
      //console.log(event.target.selectedIndex);
      var facultyName = event.target[event.target.selectedIndex].value
      this.setState({
        faculty_id: event.target[event.target.selectedIndex].id,
        faculty_name: facultyName,
      },()=> {this.props.refreshPageCount()});
    }

    componentDidMount() {
      fetch(DOMAIN + '/api/filter/semesters',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({semesters:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )
        fetch(DOMAIN + '/api/filter/faculties',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({faculties:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )
        fetch(DOMAIN + '/api/filter/years',)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({years:result.data})
            console.log(result.data)
          },
          (error) => {
              this.setState({error})
          }
        )
    }

    render() {
      let semesters = this.state.semesters.map((semester)=> {
            return (
            <option key = {semester.id} id = {semester.id}>
            {semester.name}
            </option>)
        });
        let years = this.state.years.map((year)=> {
          return (
          <option key = {year.id} id = {year.id}>
          {year.name}
          </option>)
      });
      let faculties = this.state.faculties.map((faculty)=> {
        return (
        <option key = {faculty.id} id = {faculty.id}>
        {faculty.name}
        </option>)
      });
        return (
          <div className="all-filters">
            <div className="common-filters">
                    <FormGroup className="academic-year-input">
                      <Label for="exampleSelect">Учебный год</Label>
                      <Input type="select" name="yearInput" id="exampleSelect"
                      onChange={this.handleYearSelect}>
                      <option key='0' id = '0' disabled >Выберите учебный год</option>
                          {years}
                      </Input>
                    </FormGroup>
                    <FormGroup className="semester-input">
                      <Label for="exampleSelect">Семестр</Label>
                      <Input type="select" name="semesterInput" id="exampleSelect"
                      onChange={this.handleSemesterSelect}>
                      <option key='0' disabled >Выберите семестр</option>
                        {semesters}
                      </Input>
                    </FormGroup>
                      <FormGroup className="faculty-input">
                        <Label for="exampleSelect">Факультеты</Label>
                        <Input type="select" name="facultyInput" id="exampleSelect"
                        onChange={this.handleFacultySelect}>
                          <option key='0' disabled >Выберите факультет</option>
                          {faculties}
                        </Input>
                      </FormGroup>
              </div>
              <div className="filter-download-buttons">
                    <Button variant="info" type="submit" className = "btn-submit filter-button"
                    onClick={() => this.handleClick()}>Поиск</Button>

                    <Button variant="success" type="submit" className = "btn-submit filter-button"
                    onClick={() => this.handleUpload()}>Выгрузить отчет</Button>
              </div>
          </div>
        );
    }
}
export default CommonFilters;
