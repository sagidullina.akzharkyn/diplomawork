import React, { Component } from 'react';
//import generateData from './generateData';
//import retakers from "./Mockdata/retakers.json";
import {Button} from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

class RetakeReportTable extends Component {

  render() {
    //pagination={true}
    const options = {
      sizePerPage: 20,
      prePage: 'Previous',
      nextPage: 'Next',
      //firstPage: 'First',
      //lastPage: 'Last',
      hideSizePerPage: true,
    };
    let retakers = this.props.retakers
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div >

              <div className="content">
                <BootstrapTable
                  data={retakers}
                  bordered={false}
                  striped
                  options={options}>

                  <TableHeaderColumn
                    dataField='studentId'
                    isKey
                    width="15%">
                    ID
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='lastName'
                    width="15%">
                    Фaмилия
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='firstName'
                    width="10%">
                    Имя
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='subject'
                    width="30%">
                    Предмет
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='grade1'
                    width="10%">
                    1-а
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='grade2'
                    width="10%">
                    2-а
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='finalGrade'
                    width="10%">
                    Экзамен
                  </TableHeaderColumn>

                </BootstrapTable>
              </div>
              <div className="pagination">
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickPrevious(event)}>Previous</Button>
                <Button className="pagination-page" variant="info" onClick={(event) => this.props.handleClickNext(event)}>Next</Button>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default RetakeReportTable;



  // handleClickPrevious() {
  //   if(this.state.pageCount === 1){
  //     alert("Это первая страница");
  //   }else{
  //     this.setState((prevState, { pageCount }) => ({
  //       pageCount: prevState.pageCount - 1
  //     }),()=>{
  //       var page_number = this.state.pageCount
  //       this.props.sendPageNumberToProps(page_number)
  //       console.log(this.state.pageCount);
  //     });
  //     // this.setState = {
  //     //   pageCount: this.state.pageCount --,
  //     // };
  //   }
  // }

  // handleClickNext() {
  //   this.setState((prevState, { pageCount }) => ({
  //     pageCount: prevState.pageCount + 1
  //   }),()=> {
  //     var page_number = this.state.pageCount
  //     this.props.sendPageNumberToProps(page_number)
  //     console.log(this.state.pageCount);
  //   });
  //   // this.setState = {
  //   //   pageCount: this.state.pageCount ++,
  //   // };
  // }
