import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import '../assets/styles/base.scss';

import Authorization from './Authorization';
import Main from './Main';

class App extends Component {
    render() {
        return (
            <Router>
                 <Switch>
                    <Route exact path="/login" component={Authorization} />
                    <Route path = "/main" component = {Main} />
                </Switch>
            </Router>
        );
    }
}
export default App;


// turn (
//     <BrowserRouter>
//          <Route exact path = "/login" component = {Authorization} />
//          <Route path = "/main" component = {Main} />
//     </BrowserRouter> 