import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './../../assets/styles/base.scss';

class Nav extends Component {

    render() {

        return(
            <ul className="nav">
                <li>
                    <Link to="/main/retake-report">
                            <i className="pe-7s-note2"></i>
                            <p>Отчеты по ретейкам</p>
                    </Link>
                </li>
                <li>
                    <Link to="/main/notallowed-bypoints-report">
                            <i className="pe-7s-note2"></i>
                            <p>Отчет по студентам, недопущенных к экзаменам по баллам</p>
                    </Link>
                </li>
                <li>
                    <Link to="/main/semester-performance-report">
                            <i className="pe-7s-note2"></i>
                            <p>Отчет по успеваемости за семестр</p>
                    </Link>
                </li>
                <li>
                    <Link to="/main/lost-scholarships-report">
                            <i className="pe-7s-note2"></i>
                            <p>Отчет по студентам, потерявшие стипендию</p>
                    </Link>
                </li>
                <li>
                    <Link to="/main/registered-students-report">
                            <i className="pe-7s-note2"></i>
                            <p>Отчет о регистрации студентов на дисциплины</p>
                    </Link>
                </li>
                <li>
                    <Link to="/main/retake-graph">
                            <i className="pe-7s-graph2"></i>
                            <p>График количества ретейков за семестр</p>
                    </Link>
                </li>
            </ul>
        )
    }
    isPathActive(path) {
        return this.props.location.pathname.startsWith(path);
      }
}

export default Nav;
