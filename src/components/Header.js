import React, { Component } from 'react';
import { Navbar, NavItem } from 'react-bootstrap';
import './../assets/styles/base.scss'
class Header extends Component {

    render() {
      
        return(
            <Navbar style = {{height: '75px'}}>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                        <NavItem><p style = {{color: 'black', 
                        font: 'Open Sans',paddingRight: '28px',
                        paddingTop: '5px'}}>Akzharkyn Sagidullina</p></NavItem>
                    </Navbar.Text>
                    <Navbar.Text>
                        <NavItem onClick={this.props.handleLogout}><p style = {{color: '#2a7288', 
                        font: 'Open Sans',paddingRight: '15px', textDecoration: 'underline',
                        paddingTop: '5px'}}>Log out</p></NavItem>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
export default Header;

