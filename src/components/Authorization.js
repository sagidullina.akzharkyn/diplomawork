import React, { Component } from 'react';
//import "bootstrap/dist/css/bootstrap.min.css";
import {Form, Button } from 'react-bootstrap';
import "./css/Authorization.css"
import AuthService from './AuthService';

class Authorization extends Component {
  constructor(props) {
      super(props);
      this.state = {
          email: '',
          password: ''
      };
      this.handleSubmit = this.handleSubmit.bind(this)
      this.Auth = new AuthService();
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  onInputChange = (e) => {
    this.setState({
        [e.target.name]: e.target.value,
      });
  }
 
  componentWillMount(){
    if(this.Auth.loggedIn())
        this.props.history.replace('/main');
  }

  handleSubmit(e) {
    e.preventDefault();
      
    this.Auth.login(this.state.email, this.state.password)
        .then(res =>{
          console.log(res);
          if(res.status === "error") {
            alert("Вы ввели неправильные данные")
          }
          this.props.history.replace('/main');}
        )
        .catch(err =>{
            alert(err);
        })
  }

  render() {
    return (
      <div className="App">
        <div className="brandd">
          <img src={'http://mustim09.beget.tech/storage/1004/24906-html-24e70d0jpg.jpeg'} alt="logo" className="kbtu_brand" />
        </div>
        <div className="cont card">
        <Form className="form-group" onSubmit={this.handleSubmit}>
                <Form.Group controlId="email" >
                    <Form.Label>Username</Form.Label>
                    <Form.Control name="email" type="username" placeholder="Enter username" 
                    onChange = {e => this.onInputChange(e)} required/>
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control name="password" type="password" placeholder="Password"
                    onChange = {e => this.onInputChange(e)} required/>
                </Form.Group>
                <Button variant="primary" type="submit" className = "btn-submit">Log in</Button>
            </Form>
        </div>  
      </div>
    );
  }
}

export default Authorization;
