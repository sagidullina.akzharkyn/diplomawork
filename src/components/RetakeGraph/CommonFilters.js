import React, { Component } from 'react';
import {Input, Label, FormGroup} from 'reactstrap';
import {Button } from 'react-bootstrap';
import './commonFilters.css';
//import semesters from './Mockdata/semesters.json';
//import years from './Mockdata/years.json';
//import faculties from './Mockdata/faculties.json';

const DOMAIN = 'http://18.222.209.77'

class CommonFilters extends Component {

    constructor(props) {
      super(props)
      this.state = {
        faculties: [],
        faculty_id: 1,
        error: null
      }

      this.handleFacultySelect = this.handleFacultySelect.bind(this)

    }

    handleClick() {

        var faculty_id = this.state.faculty_id
        console.log(" fac_id: " + this.state.faculty_id)
        this.props.fetchResultFromFilters(faculty_id)
    }

    handleFacultySelect(event) {
      //console.log(event.target.selectedIndex);

      this.setState({
        faculty_id: event.target[event.target.selectedIndex].id,
      });
    }

    componentDidMount() {

        fetch(DOMAIN + '/api/filter/faculties',)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({faculties:result.data})
              console.log(result.data)
            },
            (error) => {
                this.setState({error})
            }
        )

    }

    render() {
      let faculties = this.state.faculties.map((faculty)=> {
        return (
        <option key = {faculty.id} id = {faculty.id}>
        {faculty.name}
        </option>)
      });

        return (
          <div className="common-filters">
            <div>
              <FormGroup className="faculty">
                <Label for="exampleSelect">Факультеты</Label>
                <Input type="select" name="facultyInput" id="exampleSelect"
                onChange={this.handleFacultySelect}>
                <option key='0' disabled >Выберите факультет</option>
                  {faculties}
                </Input>
              </FormGroup>
            </div>
            <div>
              <Button variant="info" type="submit" className = "btn-submit filter-button"
              onClick={() => this.handleClick()}>Нарисовать график</Button>
            </div>
          </div>
        );
    }
}
export default CommonFilters;
