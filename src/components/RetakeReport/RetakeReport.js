import React, { Component } from 'react';
import RetakeReportTable from './RetakeReportTable';
import RetakeCommonFilters from './RetakeCommonFilters';
import axios from 'axios';
import AuthService from '../AuthService';
const DOMAIN = 'http://18.222.209.77'

class RetakeReport extends Component {
  constructor(props) {
    super(props)
    this.state = {
      retakers: [],
      year_id: 0,
      faculty_id: 0,
      semester_id: 0,
      year_name: '',
      semester_name: '',
      faculty_name: '',
      page_number: 1
    }
    this.handleFilterResults= this.handleFilterResults.bind(this)
    this.handleClickPrevious= this.handleClickPrevious.bind(this)
    this.handleFetch= this.handleFetch.bind(this)
    this.refreshPageCount = this.refreshPageCount.bind(this)
    this.handleClickNext = this.handleClickNext.bind(this)
    this.handleUploadedFile= this.handleUploadedFile.bind(this)
    this.handleFetchForCsvFile= this.handleFetchForCsvFile.bind(this)
    this.Auth = new AuthService()
  }

  handleFilterResults(yearId, facultyId, semesterId) {
    this.setState({
      year_id: yearId,
      faculty_id: facultyId,
      semester_id: semesterId
    }, ()=> {
      //console.log("handled filter data in retake report: year_id: " + this.state.year_id);
      this.handleFetch()
    })
  }

  handleUploadedFile(yearId, facultyId, semesterId, yearName, semesterName, facultyName) {
    this.setState({
      year_id: yearId,
      faculty_id: facultyId,
      semester_id: semesterId,
      year_name: yearName,
      semester_name: semesterName,
      faculty_name: facultyName
    }, ()=> {
      this.handleFetchForCsvFile()
    })

  }

  handleFetch() {

    var page_number = this.state.page_number
    var year_id = this.state.year_id
    var faculty_id = this.state.faculty_id
    var semester_id = this.state.semester_id
    const config = this.Auth.handleAuthHeader()

    fetch(DOMAIN + '/api/students/retake?yearId=' + year_id + '&page=' + page_number + '&facultyId=' +
        faculty_id + '&semesterId=' + semester_id,config)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({retakers:result.data})
            console.log(result.data)
          }
    )
  }

  handleFetchForCsvFile() {

    var page_number = this.state.page_number
    var year_id = this.state.year_id
    var faculty_id = this.state.faculty_id
    var semester_id = this.state.semester_id
    var year_name = this.state.year_name
    var semester_name = this.state.semester_name
    var faculty_name = this.state.faculty_name

    axios({
      url: DOMAIN + '/api/students/retake.csv?yearId=' + year_id  + '&page=' + page_number+ '&facultyId=' +
          faculty_id + '&semesterId=' + semester_id,
      method: 'GET',
      responseType: 'blob', // important
    }).then((response) => {

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;

      const fileName = `${"Отчет по студентам, недопущенных к экзаменам по баллам - " + faculty_name +
        " " + year_name  + " год " + semester_name + " семестр "}.csv`// whatever your file name .
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
    });

  }


  refreshPageCount() {
    this.setState({ page_number: 1 })
  }

  handleClickNext() {
    this.setState((prevState, { page_number }) => ({
      page_number: prevState.page_number + 1
    }),()=> {
      this.handleFetch()
      console.log(this.state.page_number);
    });
  }

  handleClickPrevious() {
    if(this.state.page_number === 1){
      alert("Это первая страница");
    }else{
      this.setState((prevState, { page_number }) => ({
        page_number: prevState.page_number - 1
      }),()=>{
        this.handleFetch()
        console.log(this.state.page_number);
      });
    }
  }

  render() {
    return (
        <div className="container-fluid">
          <div className="report-title">
            <p></p>
            <h4>Отчет по ретейкам за семестр</h4>
          </div>
        <div className="all-data">
          <div className="filters card">
            <RetakeCommonFilters fetchResultFromFilters = {this.handleFilterResults}
            getUploadedFile = {this.handleUploadedFile}
            refreshPageCount = {this.refreshPageCount}/>
          </div>
          <div className="filters card">
            <RetakeReportTable retakers = {this.state.retakers}
            handleClickNext = {this.handleClickNext}
            handleClickPrevious = {this.handleClickPrevious}/>
          </div>
        </div>
      </div>
    )
  }
}

export default RetakeReport;
