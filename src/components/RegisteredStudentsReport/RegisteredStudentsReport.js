import React, { Component } from 'react';
import RegisteredStudentsReportTable from './RegisteredStudentsReportTable';
import RegisteredStudentsCommonFilters from './RegisteredStudentsCommonFilters';
import axios from 'axios';
const DOMAIN = 'http://18.222.209.77'

class RegisteredStudentsReport extends Component {
  constructor(props) {
    super(props)
    this.state = {
      registeredStudents: [],
      year_id: 0,
      faculty_id: 0,
      semester_id: 0,
      specialty_id: 0,
      course_id: 0,
      page_number: 1,
      year_name: '',
      semester_name: '',
      faculty_name: '',
    }

    this.handleFilterResults= this.handleFilterResults.bind(this)
    this.handleClickPrevious= this.handleClickPrevious.bind(this)
    this.handleFetch= this.handleFetch.bind(this)
    this.refreshPageCount = this.refreshPageCount.bind(this)
    this.handleClickNext = this.handleClickNext.bind(this)
    this.handleUploadedFile= this.handleUploadedFile.bind(this)
    this.handleFetchForCsvFile= this.handleFetchForCsvFile.bind(this)

  }

  handleFilterResults(yearId, facultyId, semesterId, specialtyId, courseId) {
    this.setState({
      year_id: yearId,
      faculty_id: facultyId,
      semester_id: semesterId,
      specialty_id: specialtyId,
      course_id: courseId,

    }, ()=> {
      this.handleFetch()
    })
  }

  handleUploadedFile(yearId, facultyId, semesterId, specialtyId, courseId, yearName, semesterName, facultyName) {
    this.setState({
      year_id: yearId,
      faculty_id: facultyId,
      semester_id: semesterId,
      specialty_id: specialtyId,
      course_id: courseId,
      year_name: yearName,
      semester_name: semesterName,
      faculty_name: facultyName
    }, ()=> {
      this.handleFetchForCsvFile()
    })

  }

  handleFetch() {

    var page_number = this.state.page_number
    var year_id = this.state.year_id
    var faculty_id = this.state.faculty_id
    var semester_id = this.state.semester_id
    var specialty_id = this.state.specialty_id
    var course_id = this.state.course_id


    fetch(DOMAIN + '/api/students/registered?yearId=' + year_id  + '&page=' + page_number+ '&facultyId=' +
        faculty_id + '&semesterId=' + semester_id  + '&specialtyId=' + specialty_id + '&course=' + course_id
        )
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({registeredStudents:result.data})
            console.log(result.data)
          }
    )
  }

  handleFetchForCsvFile() {

    var page_number = this.state.page_number
    var year_id = this.state.year_id
    var faculty_id = this.state.faculty_id
    var semester_id = this.state.semester_id
    var specialty_id = this.state.specialty_id
    var course_id = this.state.course_id
    var year_name = this.state.year_name
    var semester_name = this.state.semester_name
    var faculty_name = this.state.faculty_name

    axios({
      url: DOMAIN + '/api/students/registered.csv?yearId=' + year_id  + '&page=' + page_number+ '&facultyId=' +
          faculty_id + '&semesterId=' + semester_id  + '&specialtyId=' + specialty_id + '&course=' + course_id,
      method: 'GET',
      responseType: 'blob', // important
    }).then((response) => {

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;

      const fileName = `${"Отчет о регистрации студентов на дисциплины - " + faculty_name +
        " " + year_name  + " год " + semester_name + " семестр "}.csv`// whatever your file name .
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
    });

  }



  refreshPageCount() {
    this.setState({ page_number: 1 })
  }

  handleClickNext() {
    this.setState((prevState, { page_number }) => ({
      page_number: prevState.page_number + 1
    }),()=> {
      this.handleFetch()
      console.log(this.state.page_number);
    });
  }

  handleClickPrevious() {
    if(this.state.page_number === 1){
      alert("Это первая страница");
    }else{
      this.setState((prevState, { page_number }) => ({
        page_number: prevState.page_number - 1
      }),()=>{
        this.handleFetch()
        console.log(this.state.page_number);
      });
    }
  }

  render() {
    return (
        <div className="container-fluid">
          <div className="report-title">
            <p></p>
            <h4>Отчет о регистрации студентов на дисциплины</h4>
          </div>
        <div className="all-data">
          <div className="filters card">
            <RegisteredStudentsCommonFilters fetchResultFromFilters = {this.handleFilterResults}
            getUploadedFile = {this.handleUploadedFile}
            refreshPageCount = {this.refreshPageCount}/>
          </div>

          <div className="filters card">
            <RegisteredStudentsReportTable registeredStudents = {this.state.registeredStudents}
            handleClickNext = {this.handleClickNext}
            handleClickPrevious = {this.handleClickPrevious}/>
          </div>
        </div>
      </div>
    )
  }
}

export default RegisteredStudentsReport;
